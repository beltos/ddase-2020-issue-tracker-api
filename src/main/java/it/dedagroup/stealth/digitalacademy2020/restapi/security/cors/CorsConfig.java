package it.dedagroup.stealth.digitalacademy2020.restapi.security.cors;

import it.dedagroup.stealth.digitalacademy2020.restapi.security.jwt.JwtSecurityProperties;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.Collections;

@Configuration
public class CorsConfig {
  private final JwtSecurityProperties jwtSecurityProperties;

  public CorsConfig(JwtSecurityProperties jwtSecurityProperties) {
    this.jwtSecurityProperties = jwtSecurityProperties;
  }

  @Bean
  public CorsConfigurationSource corsConfigurationSource() {
    final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
    CorsConfiguration configuration = new CorsConfiguration().applyPermitDefaultValues();
    configuration.setAllowedOrigins(Collections.singletonList(CorsConfiguration.ALL));
    configuration.setAllowedMethods(Collections.singletonList(CorsConfiguration.ALL));
    configuration.setAllowedHeaders(Collections.singletonList(CorsConfiguration.ALL));
    if (StringUtils.isNotBlank(jwtSecurityProperties.getHeaderKey()))
      configuration.addExposedHeader(jwtSecurityProperties.getHeaderKey());
    configuration.setAllowCredentials(true);
    source.registerCorsConfiguration("/**", configuration);
    return source;
  }
}
