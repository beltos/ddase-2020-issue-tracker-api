package it.dedagroup.stealth.digitalacademy2020.restapi.user;

import it.dedagroup.stealth.digitalacademy2020.restapi.exception.NotFoundException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
public class UserApiController implements UserApi {

  private final UserService service;

  public UserApiController(UserService service) {
    this.service = service;
  }

  @Override
  public List<UserDto> getAll(String filter) {
    return service.getAll(filter);
  }

  @Override
  public UserDto getOne(@PathVariable Long id) throws NotFoundException {
    return service.getOne(id);
  }

  @Override
  public UserDto create(@RequestBody @Valid UserDto dto) throws UsernameTakenException {
    return service.create(dto);
  }

  @Override
  public UserDto update(@PathVariable Long id, @RequestBody @Valid UserDto dto) throws UsernameTakenException, NotFoundException {
    return service.update(id, dto);
  }
}
