package it.dedagroup.stealth.digitalacademy2020.restapi.security.jwt;

import io.jsonwebtoken.*;
import it.dedagroup.stealth.digitalacademy2020.restapi.user.User;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

import java.lang.reflect.Array;
import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static it.dedagroup.stealth.digitalacademy2020.restapi.security.jwt.JwtConstants.*;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class JwtUtils {
  /**
   * Build a JWT for the user
   *
   * @param user           the user
   * @param expirationTime the expiration time in milliseconds to set the validity date from the current date
   * @param secretKey      the JWT secret key
   * @return the JWT as a string
   */
  public static String buildToken(User user, long expirationTime, String secretKey) {
    return buildToken(user, expirationTime, secretKey, Collections.emptyMap());
  }

  /**
   * Build a JWT for the user
   *
   * @param user             the user
   * @param expirationTime   the expiration time in milliseconds to set the validity date from the current date
   * @param secretKey        the JWT secret key
   * @param additionalClaims additional claims to add
   * @return the JWT as a string
   */
  public static String buildToken(User user, long expirationTime, String secretKey, Map<String, Object> additionalClaims) {
    return Jwts.builder()
        .setSubject(user.getUsername())
        .setExpiration(new Date(System.currentTimeMillis() + expirationTime))
        .addClaims(buildClaims(user))
        .addClaims(additionalClaims)
        .signWith(SignatureAlgorithm.HS512, base64EncodeSecretKey(secretKey))
//        .compressWith(CompressionCodecs.DEFLATE)
        .compact();
  }

  /**
   * Refresh a JWT by extending the validity date.<br />
   * <strong>Warning: the user should be re-validated against the database before calling this method.</strong>
   *
   * @param token          the token
   * @param secretKey      the JWT secret key
   * @param expirationTime the expiration time in milliseconds to extend the validity date from the current date
   * @return the JWT as a string
   */
  public static String refreshToken(String token, String secretKey, long expirationTime) {
    Claims claims = parseClaims(token, secretKey);
    return refreshToken(claims, expirationTime, secretKey);
  }

  /**
   * Parse the token and get the claims
   *
   * @param token     the token
   * @param secretKey the JWT secret key
   * @return the claims
   */
  public static Claims parseClaims(String token, String secretKey) {
    return Jwts.parser()
        .setSigningKey(base64EncodeSecretKey(secretKey))
        .parseClaimsJws(token)
        .getBody();
  }

  /**
   * Work-around to read the claims without knowing the signing key<br />
   * See https://github.com/jwtk/jjwt/issues/205#issuecomment-288818274
   *
   * @param token      the encoded token to decode
   * @param claim      the claim name
   * @param returnType the value type
   * @return the claim value
   */
  public static <T> T getClaim(String token, String claim, Class<T> returnType) {
    final T[] result = (T[]) Array.newInstance(returnType, 1);
    try {
      SigningKeyResolver signingKeyResolver = new SigningKeyResolverAdapter() {
        @Override
        public Key resolveSigningKey(JwsHeader header, Claims claims) {
          result[0] = claims.get(claim, returnType);
          return null;
        }
      };
      Jwts.parser().setSigningKeyResolver(signingKeyResolver).parseClaimsJws(token).getBody();
    } catch (IllegalArgumentException ignored) {
      // no signing key on client. We trust that this JWT came from the server and has been verified there
    }
    return result[0];
  }

  private static String refreshToken(Claims claims, long expirationTime, String secretKey) {
    return Jwts.builder()
        .setClaims(claims)
        .setExpiration(new Date(System.currentTimeMillis() + expirationTime))
        .signWith(SignatureAlgorithm.HS512, base64EncodeSecretKey(secretKey))
//        .compressWith(CompressionCodecs.DEFLATE)
        .compact();
  }

  private static Map<String, Object> buildClaims(User user) {
    return Stream.of(
        new AbstractMap.SimpleEntry<>(PUBLIC_CLAIM_FULL_NAME, user.getFullName()),
        new AbstractMap.SimpleEntry<>(getPrivateClaimName(PRIVATE_CLAIM_USER_ID), user.getId()),
        new AbstractMap.SimpleEntry<>(getPrivateClaimName(PRIVATE_CLAIM_ROLES), getRoles(user)))
        .collect(Collectors.toMap(
            Map.Entry::getKey,
            Map.Entry::getValue));
  }

  public static String getPrivateClaimName(String claim) {
    return PRIVATE_CLAIMS_PREFIX + "." + claim;
  }

  private static List<String> getRoles(User user) {
    return user.getAuthorities().stream()
        .map(GrantedAuthority::getAuthority)
        .collect(Collectors.toList());
  }

  private static String base64EncodeSecretKey(String secretKey) {
    return Base64.getEncoder().encodeToString(secretKey.getBytes(StandardCharsets.UTF_8));
  }

  static <T> Optional<T> getOptionalClaim(Claims jwt, String claimName, Class<T> requiredType) {
    return Optional.ofNullable(jwt.get(claimName, requiredType));
  }
}
