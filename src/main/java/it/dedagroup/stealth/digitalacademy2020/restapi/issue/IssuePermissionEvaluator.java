package it.dedagroup.stealth.digitalacademy2020.restapi.issue;

import it.dedagroup.stealth.digitalacademy2020.restapi.security.permissionevaluator.PermissionEvaluator;
import it.dedagroup.stealth.digitalacademy2020.restapi.user.User;
import it.dedagroup.stealth.digitalacademy2020.restapi.user.UserRepository;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Objects;
import java.util.Optional;

@Component
public class IssuePermissionEvaluator implements PermissionEvaluator {

  private static final String PERMISSION_DELETE = "delete";

  private final UserRepository userRepository;
  private final IssueRepository issueRepository;

  public IssuePermissionEvaluator(UserRepository userRepository, IssueRepository issueRepository) {
    this.userRepository = userRepository;
    this.issueRepository = issueRepository;
  }

  @Override
  public boolean canManage(String targetType) {
    return IssueDto.class.getName().equals(targetType);
  }

  @Override
  public boolean hasPermission(Authentication authentication, Object targetDomainObject, Object permission) {
    throw new UnsupportedOperationException();
  }

  @Override
  public boolean hasPermission(Authentication authentication, Serializable targetId, String targetType, Object permission) {
    if (!(targetId instanceof Long))
      return false;
    Optional<User> currentUser = userRepository.findByUsername(authentication.getName());
    Optional<Issue> issue = issueRepository.findById((Long) targetId);
    if(!issue.isPresent()) return true;
    if(!currentUser.isPresent()) return false;
    return Objects.equals(issue.get().getReporter(), currentUser.get()) ||
        (!PERMISSION_DELETE.equals(permission) && Objects.equals(issue.get().getAssignee(), currentUser.get()));
  }
}
