package it.dedagroup.stealth.digitalacademy2020.restapi.user;

import it.dedagroup.stealth.digitalacademy2020.restapi.security.permissionevaluator.PermissionEvaluator;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Objects;
import java.util.Optional;

@Component
public class UserPermissionEvaluator implements PermissionEvaluator {

  private final UserRepository userRepository;

  public UserPermissionEvaluator(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  @Override
  public boolean canManage(String targetType) {
    return UserDto.class.getName().equals(targetType);
  }

  @Override
  public boolean hasPermission(Authentication authentication, Object targetDomainObject, Object permission) {
    throw new UnsupportedOperationException();
  }

  @Override
  public boolean hasPermission(Authentication authentication, Serializable targetId, String targetType, Object permission) {
    if (!(targetId instanceof Long))
      return false;
    Optional<User> user = userRepository.findById((Long) targetId);
    if(!user.isPresent()) return true;
    return userRepository.findByUsername(authentication.getName())
        .map(u -> Objects.equals(targetId, u.getId()))
        .orElse(false);
  }
}
