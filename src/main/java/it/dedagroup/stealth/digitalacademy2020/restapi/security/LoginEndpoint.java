package it.dedagroup.stealth.digitalacademy2020.restapi.security;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/login")
public class LoginEndpoint {
  @PostMapping
  public void get() {
    // dummy endpoint for login
  }
}
