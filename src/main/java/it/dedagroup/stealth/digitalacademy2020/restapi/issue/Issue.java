package it.dedagroup.stealth.digitalacademy2020.restapi.issue;

import it.dedagroup.stealth.digitalacademy2020.restapi.user.User;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.AbstractPersistable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@EntityListeners(AuditingEntityListener.class)
public class Issue extends AbstractPersistable<Long> {
  @NotNull
  private String title;
  @NotNull
  private String description;
  @NotNull
  private IssueStatus status;
  @ManyToOne
  private User reporter;
  @ManyToOne
  private User assignee;

  @CreatedDate
  private LocalDateTime createdAt;
  @LastModifiedDate
  private LocalDateTime updatedAt;
}
