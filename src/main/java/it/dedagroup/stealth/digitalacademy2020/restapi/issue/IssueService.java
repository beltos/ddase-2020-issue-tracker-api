package it.dedagroup.stealth.digitalacademy2020.restapi.issue;

import it.dedagroup.stealth.digitalacademy2020.restapi.exception.NotFoundException;
import it.dedagroup.stealth.digitalacademy2020.restapi.user.User;
import it.dedagroup.stealth.digitalacademy2020.restapi.user.UserRepository;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class IssueService {

  private final IssueRepository repository;
  private final UserRepository userRepository;

  public IssueService(IssueRepository repository, UserRepository userRepository) {
    this.repository = repository;
    this.userRepository = userRepository;
  }

  public List<IssueDto> getAll(IssueStatus status, Long reporterId, Long assigneeId) {
    Specification<Issue> spec = buildIssueSpecification(status, reporterId, assigneeId);
    return repository.findAll(spec).stream()
        .map(this::toDto)
        .collect(Collectors.toList());
  }

  private static Specification<Issue> buildIssueSpecification(IssueStatus status, Long reporterId, Long assigneeId) {
    return (root, q, cb) -> {
      List<Predicate> predicates = new ArrayList<>();
      if (status != null)
        predicates.add(cb.equal(root.get("status"), status));
      if (reporterId != null)
        predicates.add(cb.equal(root.get("reporter").get("id"), reporterId));
      if (assigneeId != null)
        predicates.add(cb.equal(root.get("assignee").get("id"), assigneeId));
      return cb.and(predicates.toArray(new Predicate[0]));
    };
  }

  public IssueDto getOne(Long id) throws NotFoundException {
    return repository.findById(id)
        .map(this::toDto)
        .orElseThrow(NotFoundException::new);
  }

  public IssueDto create(IssueDto dto) throws UserNotFoundException {
    Issue issue = fromDto(dto);
    getCurrentUser().ifPresent(issue::setReporter);
    return toDto(repository.saveAndFlush(issue));
  }

  public Optional<User> getCurrentUser() {
    return Optional.ofNullable(SecurityContextHolder.getContext())
        .map(SecurityContext::getAuthentication)
        .filter(Authentication::isAuthenticated)
        .map(Authentication::getName)
        .flatMap(userRepository::findByUsername);
  }

  public IssueDto update(Long id, IssueDto dto) throws NotFoundException, UserNotFoundException {
    Issue issue = repository.findById(id).orElseThrow(NotFoundException::new);

    setFromDto(dto, issue);
    repository.saveAndFlush(issue);
    return toDto(issue);
  }

  public void delete(Long id) throws NotFoundException {
    Issue issue = repository.findById(id).orElseThrow(NotFoundException::new);
    repository.delete(issue);
  }

  private Issue fromDto(IssueDto dto) throws UserNotFoundException {
    Issue issue = new Issue();
    setFromDto(dto, issue);
    return issue;
  }

  private void setFromDto(IssueDto source, Issue target) throws UserNotFoundException {
    if (source.getAssigneeId() != null) {
      User assignee = userRepository.findById(source.getAssigneeId()).orElseThrow(UserNotFoundException::new);
      target.setAssignee(assignee);
    }
    target.setTitle(source.getTitle());
    target.setDescription(source.getDescription());
    target.setStatus(source.getStatus());
  }

  private IssueDto toDto(Issue issue) {
    IssueDto dto = new IssueDto();
    dto.setId(issue.getId());
    dto.setTitle(issue.getTitle());
    dto.setDescription(issue.getDescription());
    dto.setStatus(issue.getStatus());
    if (issue.getReporter() != null)
      dto.setReporterId(issue.getReporter().getId());
    if (issue.getAssignee() != null)
      dto.setAssigneeId(issue.getAssignee().getId());
    dto.setCreatedAt(issue.getCreatedAt());
    dto.setUpdatedAt(issue.getUpdatedAt());
    return dto;
  }
}
