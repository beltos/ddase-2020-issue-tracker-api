package it.dedagroup.stealth.digitalacademy2020.restapi.security.jwt;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "security.jwt")
@Getter
@Setter
public class JwtSecurityProperties {
  private String secret;
  private long expirationTime;
  private String tokenPrefix;
  private String headerKey;
}