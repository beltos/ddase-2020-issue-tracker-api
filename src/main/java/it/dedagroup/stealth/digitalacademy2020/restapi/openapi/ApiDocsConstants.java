package it.dedagroup.stealth.digitalacademy2020.restapi.openapi;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ApiDocsConstants {
  public static final String API_DOCS_YML_RESOURCE_PATH = "/api-docs.yml";
}
