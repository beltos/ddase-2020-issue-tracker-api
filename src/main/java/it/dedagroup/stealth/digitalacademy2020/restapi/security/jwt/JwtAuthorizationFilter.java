package it.dedagroup.stealth.digitalacademy2020.restapi.security.jwt;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static it.dedagroup.stealth.digitalacademy2020.restapi.security.jwt.JwtConstants.PRIVATE_CLAIM_ROLES;
import static it.dedagroup.stealth.digitalacademy2020.restapi.security.jwt.JwtUtils.getPrivateClaimName;
import static it.dedagroup.stealth.digitalacademy2020.restapi.security.jwt.JwtUtils.parseClaims;

@Slf4j
public class JwtAuthorizationFilter extends BasicAuthenticationFilter {

  private final JwtSecurityProperties properties;

  public JwtAuthorizationFilter(AuthenticationManager authManager, JwtSecurityProperties properties) {
    super(authManager);
    this.properties = properties;
  }

  /**
   * Checks the validity of the JWT, if present in the headers of the HttpServletRequest
   *
   * @param req   the HttpServletRequest
   * @param res   the HttpServletResponse
   * @param chain the FilterChain
   */
  @Override
  protected void doFilterInternal(HttpServletRequest req,
                                  HttpServletResponse res,
                                  FilterChain chain) throws IOException, ServletException {
    String header = req.getHeader(properties.getHeaderKey());

    if (header == null || !header.startsWith(properties.getTokenPrefix())) {
      chain.doFilter(req, res);
      return;
    }

    Authentication authenticationToken;
    try {
      authenticationToken = getAuthentication(req);
    } catch (ExpiredJwtException e) {
      log.debug("Received expired JWT from user {}", e.getClaims().getSubject());
      chain.doFilter(req, res);
      return;
    }

    SecurityContextHolder.getContext().setAuthentication(authenticationToken);
    chain.doFilter(req, res);
  }

  /**
   * Validates the JWT present in the HttpServletRequest and builds a Authentication
   *
   * @param request the HttpServletRequest
   * @return a Authentication
   */
  @SuppressWarnings("unchecked")
  private Authentication getAuthentication(HttpServletRequest request) {
    String tokenHeader = request.getHeader(properties.getHeaderKey());
    if (tokenHeader != null) {
      String token = tokenHeader.replace(properties.getTokenPrefix(), "");
      Claims jwt = parseClaims(token, properties.getSecret());
      String user = jwt.getSubject();
      if (user != null) {
        Collection<String> roles = (List<String>) JwtUtils.getOptionalClaim(jwt, getPrivateClaimName(PRIVATE_CLAIM_ROLES), List.class).orElse(Collections.emptyList());
        Collection<GrantedAuthority> authorities = roles.stream()
            .map(SimpleGrantedAuthority::new)
            .collect(Collectors.toSet());
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(user, null, authorities);
        authenticationToken.setDetails(token);
        return authenticationToken;
      }
      return null;
    }
    return null;
  }
}