package it.dedagroup.stealth.digitalacademy2020.restapi.openapi;

import io.swagger.v3.oas.models.OpenAPI;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
public class ApiDocsController {

  private final ApiDocsService service;

  public ApiDocsController(ApiDocsService service) {
    this.service = service;
  }

  @RequestMapping("/api-docs.yml")
  public ResponseEntity<String> getApiDocs(HttpServletRequest request) throws Exception {
    OpenAPI openAPI = service.getOpenApi(request);

    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.valueOf("text/yaml"));
    return ResponseEntity.ok()
        .headers(headers)
        .body(OpenApiUtils.serializeOpenAPIToYaml(openAPI));
  }
}
