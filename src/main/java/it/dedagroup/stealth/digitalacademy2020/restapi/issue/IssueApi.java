package it.dedagroup.stealth.digitalacademy2020.restapi.issue;

import it.dedagroup.stealth.digitalacademy2020.restapi.exception.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RequestMapping("/v1/issues")
public interface IssueApi {

  int RESPONSE_STATUS_USER_NOT_FOUND = 461;

  @GetMapping
  List<IssueDto> getAll(@RequestParam(required = false) IssueStatus status,
                        @RequestParam(required = false) Long reporterId,
                        @RequestParam(required = false) Long assigneeId);

  @GetMapping("/{id}")
  IssueDto getOne(@PathVariable Long id) throws NotFoundException;

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  IssueDto create(@RequestBody @Valid IssueDto dto) throws UserNotFoundException;

  @PutMapping("/{id}")
  @PreAuthorize("hasPermission(#id, 'it.dedagroup.stealth.digitalacademy2020.restapi.issue.IssueDto', 'update')")
  IssueDto update(@PathVariable Long id, @RequestBody @Valid IssueDto dto) throws NotFoundException, UserNotFoundException;

  @DeleteMapping("/{id}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  @PreAuthorize("hasPermission(#id, 'it.dedagroup.stealth.digitalacademy2020.restapi.issue.IssueDto', 'delete')")
  void delete(@PathVariable Long id) throws NotFoundException;

  @ExceptionHandler(NotFoundException.class)
  @ResponseStatus(HttpStatus.NOT_FOUND)
  default void handleNotFoundException() {
  }

  @ExceptionHandler(UserNotFoundException.class)
  default ResponseEntity<?> handleUserNotFoundException() {
    return ResponseEntity.status(RESPONSE_STATUS_USER_NOT_FOUND).build();
  }
}
