package it.dedagroup.stealth.digitalacademy2020.restapi.issue;

import it.dedagroup.stealth.digitalacademy2020.restapi.exception.NotFoundException;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.beans.PropertyEditorSupport;
import java.util.List;

@RestController
public class IssueApiController implements IssueApi {

  private final IssueService service;

  public IssueApiController(IssueService service) {
    this.service = service;
  }

  @Override
  public List<IssueDto> getAll(IssueStatus status, Long reporterId, Long assigneeId) {
    return service.getAll(status, reporterId, assigneeId);
  }

  @InitBinder
  public void initBinder(WebDataBinder webDataBinder) {
    webDataBinder.registerCustomEditor(IssueStatus.class, new PropertyEditorSupport() {
      @Override
      public void setAsText(String text) throws IllegalArgumentException {
        setValue(IssueStatus.fromString(text));
      }
    });
  }

  @Override
  public IssueDto getOne(Long id) throws NotFoundException {
    return service.getOne(id);
  }

  @Override
  public IssueDto create(@Valid IssueDto dto) throws UserNotFoundException {
    return service.create(dto);
  }

  @Override
  public IssueDto update(Long id, @Valid IssueDto dto) throws NotFoundException, UserNotFoundException {
    return service.update(id, dto);
  }

  @Override
  public void delete(Long id) throws NotFoundException {
    service.delete(id);
  }
}
