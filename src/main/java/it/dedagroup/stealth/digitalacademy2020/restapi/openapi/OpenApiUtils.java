package it.dedagroup.stealth.digitalacademy2020.restapi.openapi;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.swagger.v3.core.util.Yaml;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.servers.Server;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class OpenApiUtils {
  public static OpenAPI readOpenApi(InputStream inputStream) throws IOException {
    return Yaml
        .mapper()
        .readValue(inputStream, OpenAPI.class);
  }

  public static void replaceServerUrlInOpenApiDefinition(OpenAPI openAPI, OpenApiProperties properties, HttpServletRequest request) {
    openAPI.getServers().clear();
    openAPI.getServers().add(new Server().url(getActualUrl(properties, request)));
  }

  private static String getActualUrl(OpenApiProperties properties, HttpServletRequest request) {
    return properties.getServer().getUrl()
        .orElseGet(() -> getContextUrl(request));
  }

  private static String getContextUrl(HttpServletRequest request) {
    return URI
        .create(request.getRequestURL().toString())
        .resolve(request.getContextPath())
        .toString();
  }

  public static String serializeOpenAPIToYaml(OpenAPI openAPI) throws JsonProcessingException {
    return Yaml
        .mapper()
        .writeValueAsString(openAPI);
  }
}
