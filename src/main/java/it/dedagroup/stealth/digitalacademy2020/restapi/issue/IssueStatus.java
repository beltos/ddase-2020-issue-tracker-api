package it.dedagroup.stealth.digitalacademy2020.restapi.issue;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum IssueStatus {
  OPEN, CLOSED;

  @JsonValue
  public String toString() {
    return this.name().toLowerCase();
  }

  @JsonCreator
  public static IssueStatus fromString(String string) {
    for (IssueStatus b : IssueStatus.values()) {
      if (b.name().equalsIgnoreCase(string)) {
        return b;
      }
    }
    return null;
  }
}
