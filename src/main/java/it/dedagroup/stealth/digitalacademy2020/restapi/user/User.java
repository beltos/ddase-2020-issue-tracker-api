package it.dedagroup.stealth.digitalacademy2020.restapi.user;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.AbstractPersistable;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.Collection;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = "username"))
@Getter
@Setter
public class User extends AbstractPersistable<Long> implements UserDetails {

  private static final GrantedAuthority[] DEFAULT_AUTHORITIES = {
      new SimpleGrantedAuthority("USER")
  };

  @NotNull
  private String username;
  @NotNull
  private String password;
  private String firstName;
  private String lastName;

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    return Arrays.asList(DEFAULT_AUTHORITIES);
  }

  @Override
  public boolean isEnabled() {
    return true;
  }

  @Override
  public boolean isAccountNonLocked() {
    return true;
  }

  @Override
  public boolean isAccountNonExpired() {
    return true;
  }

  @Override
  public boolean isCredentialsNonExpired() {
    return true;
  }

  public String getFullName() {
    return (StringUtils.isNotBlank(firstName) ? firstName + " " : "") +
        (StringUtils.isNotBlank(lastName) ? lastName : "");
  }
}
