package it.dedagroup.stealth.digitalacademy2020.restapi.security.jwt;

import it.dedagroup.stealth.digitalacademy2020.restapi.user.User;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class JwtAuthenticationFilter extends BasicAuthenticationFilter {

  private final AuthenticationEntryPoint authenticationEntryPoint;
  private final String pathLogin;
  private final JwtSecurityProperties properties;

  public JwtAuthenticationFilter(AuthenticationManager authenticationManager, AuthenticationEntryPoint authenticationEntryPoint, String pathLogin, JwtSecurityProperties properties) {
    super(authenticationManager);
    this.authenticationEntryPoint = authenticationEntryPoint;
    this.pathLogin = pathLogin;
    this.properties = properties;
  }

  @Override
  protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
    if (request.getServletPath().equals(pathLogin)) {
      super.doFilterInternal(request, response, chain);
    } else {
      chain.doFilter(request, response);
    }
  }

  @Override
  protected void onSuccessfulAuthentication(HttpServletRequest req, HttpServletResponse res, Authentication auth) throws IOException {
    UsernamePasswordAuthenticationToken authenticationToken = (UsernamePasswordAuthenticationToken) auth;
    User user = (User) authenticationToken.getPrincipal();
    String token = JwtUtils.buildToken(user, properties.getExpirationTime(), properties.getSecret());
    res.addHeader(properties.getHeaderKey(), properties.getTokenPrefix() + token);
  }

  @Override
  protected AuthenticationEntryPoint getAuthenticationEntryPoint() {
    return authenticationEntryPoint;
  }
}