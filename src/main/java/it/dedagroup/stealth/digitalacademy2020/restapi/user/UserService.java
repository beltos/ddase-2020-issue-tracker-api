package it.dedagroup.stealth.digitalacademy2020.restapi.user;

import it.dedagroup.stealth.digitalacademy2020.restapi.exception.NotFoundException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class UserService {
  private final UserRepository repository;
  private final PasswordEncoder passwordEncoder;

  public UserService(UserRepository repository, PasswordEncoder passwordEncoder) {
    this.repository = repository;
    this.passwordEncoder = passwordEncoder;
  }

  public List<UserDto> getAll(String filter) {
    Specification<User> spec = buildUserSpecification(filter);
    return repository.findAll(spec).stream()
        .map(this::toDto)
        .collect(Collectors.toList());
  }

  private static Specification<User> buildUserSpecification(String filter) {
    return (root, q, cb) -> {
      List<Predicate> predicates = new ArrayList<>();
      if (StringUtils.isNotBlank(filter))
        predicates.addAll(Arrays.stream(filter.split(" ")).map(
            s -> cb.or(
                Stream.of("username", "firstName", "lastName")
                    .map(field -> cb.like(cb.lower(root.get(field)), "%" + s.toLowerCase() + "%"))
                    .toArray(Predicate[]::new)
            )
        ).collect(Collectors.toList()));
      return cb.and(predicates.toArray(new Predicate[0]));
    };
  }

  public UserDto getOne(Long id) throws NotFoundException {
    return repository.findById(id)
        .map(this::toDto)
        .orElseThrow(NotFoundException::new);
  }

  public UserDto create(UserDto dto) throws UsernameTakenException {
    if (repository.findByUsername(dto.getUsername()).isPresent()) {
      throw new UsernameTakenException();
    }
    return toDto(repository.saveAndFlush(fromDto(dto)));
  }

  public UserDto update(Long id, UserDto dto) throws UsernameTakenException, NotFoundException {
    User user = repository.findById(id).orElseThrow(NotFoundException::new);

    if (!user.getUsername().equals(dto.getUsername())) {
      if (repository.findByUsername(dto.getUsername()).isPresent()) {
        throw new UsernameTakenException();
      }
    }

    setFromDto(dto, user);
    repository.saveAndFlush(user);
    return toDto(user);
  }

  private User fromDto(UserDto dto) {
    User user = new User();
    setFromDto(dto, user);
    return user;
  }

  private void setFromDto(UserDto source, User target) {
    target.setUsername(source.getUsername());
    if (source.getPassword() != null)
      target.setPassword(passwordEncoder.encode(source.getPassword()));
    target.setFirstName(source.getFirstName());
    target.setLastName(source.getLastName());
  }

  private UserDto toDto(User user) {
    UserDto dto = new UserDto();
    dto.setId(user.getId());
    dto.setUsername(user.getUsername());
    dto.setFirstName(user.getFirstName());
    dto.setLastName(user.getLastName());
    dto.setFullName(user.getFullName());
    return dto;
  }
}
