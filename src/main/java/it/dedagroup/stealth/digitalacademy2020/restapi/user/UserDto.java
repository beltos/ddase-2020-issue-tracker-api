package it.dedagroup.stealth.digitalacademy2020.restapi.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class UserDto {
  @JsonProperty(access = JsonProperty.Access.READ_ONLY)
  private Long id;
  @NotNull
  private String username;
  @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
  private String password;
  private String firstName;
  private String lastName;
  @JsonProperty(access = JsonProperty.Access.READ_ONLY)
  private String fullName;
}
