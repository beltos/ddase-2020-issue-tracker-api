package it.dedagroup.stealth.digitalacademy2020.restapi.security.jwt;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class JwtConstants {
  public static final String PRIVATE_CLAIMS_PREFIX = "ddg";
  public static final String PRIVATE_CLAIM_USER_ID = "userId";
  public static final String PRIVATE_CLAIM_ROLES = "roles";
  public static final String PUBLIC_CLAIM_FULL_NAME = "name";
}
