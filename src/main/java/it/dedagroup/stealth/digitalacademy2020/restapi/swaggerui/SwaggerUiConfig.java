package it.dedagroup.stealth.digitalacademy2020.restapi.swaggerui;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.webjars.WebJarAssetLocator;

@Configuration
public class SwaggerUiConfig implements WebMvcConfigurer {

  @Override
  public void addResourceHandlers(ResourceHandlerRegistry registry) {
    registry.addResourceHandler("/swagger-ui/**").addResourceLocations("classpath:" + getPathToSwaggerUi());
    registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:META-INF/resources/webjars/");
  }

  private String getPathToSwaggerUi() {
    WebJarAssetLocator locator = new WebJarAssetLocator();
    return locator.getFullPath("swagger-ui", "/index.html").replaceAll("index.html$", "");
  }
}