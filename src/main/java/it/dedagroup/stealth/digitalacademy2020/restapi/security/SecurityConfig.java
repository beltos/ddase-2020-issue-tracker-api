package it.dedagroup.stealth.digitalacademy2020.restapi.security;

import it.dedagroup.stealth.digitalacademy2020.restapi.security.jwt.JwtAuthenticationFilter;
import it.dedagroup.stealth.digitalacademy2020.restapi.security.jwt.JwtAuthorizationFilter;
import it.dedagroup.stealth.digitalacademy2020.restapi.security.jwt.JwtSecurityProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.HttpStatusEntryPoint;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;
import org.springframework.web.filter.GenericFilterBean;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

  private static final String PATH_JWT_LOGIN = "/v1/login";
  private static final String[] PATTERNS_SWAGGER_UI = {
      "/swagger-ui/**", "/api-docs.yml"
  };

  private final JwtSecurityProperties jwtSecurityProperties;

  public SecurityConfig(JwtSecurityProperties jwtSecurityProperties) {
    this.jwtSecurityProperties = jwtSecurityProperties;
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http
        .antMatcher("/**")
        .csrf().disable()
        .cors()
        .and()
        .addFilterAfter(jwtAuthenticationProcessingFilter(), AbstractPreAuthenticatedProcessingFilter.class)
        .addFilter(new JwtAuthorizationFilter(authenticationManager(), jwtSecurityProperties))
        .exceptionHandling().authenticationEntryPoint(authenticationEntryPoint())
        .and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS) // this disables session creation on Spring Security
        .and().authorizeRequests()
        .antMatchers(HttpMethod.GET, "/").permitAll()
        .antMatchers(HttpMethod.POST, "/v1/users").permitAll()
        .antMatchers(HttpMethod.GET, PATTERNS_SWAGGER_UI).permitAll()
        .anyRequest().authenticated();
  }

  @Bean
  public AuthenticationEntryPoint authenticationEntryPoint() {
    return new HttpStatusEntryPoint(HttpStatus.UNAUTHORIZED);
  }

  @Bean
  public GenericFilterBean jwtAuthenticationProcessingFilter() throws Exception {
    return new JwtAuthenticationFilter(authenticationManager(), authenticationEntryPoint(), PATH_JWT_LOGIN, jwtSecurityProperties);
  }

  @Bean
  public PasswordEncoder passwordEncoder() {
    return PasswordEncoderFactories.createDelegatingPasswordEncoder();
  }
}
