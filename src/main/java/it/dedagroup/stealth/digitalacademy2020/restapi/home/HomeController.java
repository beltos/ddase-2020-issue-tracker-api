package it.dedagroup.stealth.digitalacademy2020.restapi.home;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;

@Controller
public class HomeController {

  private static final String API_DOCS_FILENAME = "api-docs.yml";

  @GetMapping
  public String home(HttpServletRequest request) {
    return "redirect:swagger-ui/index.html?url=" + getContextUrl(request) + API_DOCS_FILENAME;
  }

  private static String getContextUrl(HttpServletRequest request) {
    return URI
        .create(request.getRequestURL().toString())
        .resolve(request.getContextPath())
        .toString();
  }
}
