package it.dedagroup.stealth.digitalacademy2020.restapi.security.permissionevaluator;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.security.access.expression.method.DefaultMethodSecurityExpressionHandler;
import org.springframework.security.access.expression.method.MethodSecurityExpressionHandler;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.method.configuration.GlobalMethodSecurityConfiguration;

@ConditionalOnBean(CompositePermissionEvaluator.class)
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class CompositePermissionEvaluatorConfig extends GlobalMethodSecurityConfiguration {

  private final CompositePermissionEvaluator compositePermissionEvaluator;

  public CompositePermissionEvaluatorConfig(CompositePermissionEvaluator compositePermissionEvaluator) {
    this.compositePermissionEvaluator = compositePermissionEvaluator;
  }

  @Override
  protected MethodSecurityExpressionHandler createExpressionHandler() {
    DefaultMethodSecurityExpressionHandler expressionHandler = new DefaultMethodSecurityExpressionHandler();
    expressionHandler.setPermissionEvaluator(compositePermissionEvaluator);
    return expressionHandler;
  }
}