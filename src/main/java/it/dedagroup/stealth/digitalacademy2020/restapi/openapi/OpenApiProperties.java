package it.dedagroup.stealth.digitalacademy2020.restapi.openapi;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@ConfigurationProperties(prefix = "openapi")
@Data
public class OpenApiProperties {
  private Server server = new Server();

  @Data
  public static class Server {
    private Optional<String> url = Optional.empty();
  }
}
