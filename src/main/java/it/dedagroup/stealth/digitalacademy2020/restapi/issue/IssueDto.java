package it.dedagroup.stealth.digitalacademy2020.restapi.issue;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@Setter
public class IssueDto {
  @JsonProperty(access = JsonProperty.Access.READ_ONLY)
  private Long id;
  @NotNull
  private String title;
  @NotNull
  private String description;
  @NotNull
  private IssueStatus status;
  @JsonProperty(access = JsonProperty.Access.READ_ONLY)
  private Long reporterId;
  private Long assigneeId;
  @JsonProperty(access = JsonProperty.Access.READ_ONLY)
  private LocalDateTime createdAt;
  @JsonProperty(access = JsonProperty.Access.READ_ONLY)
  private LocalDateTime updatedAt;
}
