package it.dedagroup.stealth.digitalacademy2020.restapi.openapi;

import io.swagger.v3.oas.models.OpenAPI;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

import static it.dedagroup.stealth.digitalacademy2020.restapi.openapi.ApiDocsConstants.API_DOCS_YML_RESOURCE_PATH;

@Component
public class ApiDocsService {

  private final OpenApiProperties properties;

  public ApiDocsService(OpenApiProperties properties) {
    this.properties = properties;
  }

  public OpenAPI getOpenApi(HttpServletRequest request) throws IOException {
    OpenAPI openAPI = OpenApiUtils.readOpenApi(new ClassPathResource(API_DOCS_YML_RESOURCE_PATH).getInputStream());

    OpenApiUtils.replaceServerUrlInOpenApiDefinition(openAPI, properties, request);

    return openAPI;
  }
}
