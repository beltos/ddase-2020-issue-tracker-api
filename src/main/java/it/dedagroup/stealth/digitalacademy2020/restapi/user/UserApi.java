package it.dedagroup.stealth.digitalacademy2020.restapi.user;

import it.dedagroup.stealth.digitalacademy2020.restapi.exception.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RequestMapping("/v1/users")
public interface UserApi {
  @GetMapping
  List<UserDto> getAll(@RequestParam(required = false) String filter);

  @GetMapping("/{id}")
  UserDto getOne(@PathVariable Long id) throws NotFoundException;

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  UserDto create(@RequestBody @Valid UserDto dto) throws UsernameTakenException;

  @PutMapping("/{id}")
  @PreAuthorize("hasPermission(#id, 'it.dedagroup.stealth.digitalacademy2020.restapi.user.UserDto', 'update')")
  UserDto update(@PathVariable Long id, @RequestBody @Valid UserDto dto) throws UsernameTakenException, NotFoundException;

  @ExceptionHandler(UsernameTakenException.class)
  @ResponseStatus(HttpStatus.CONFLICT)
  default void handleUsernameTakenException() {
  }

  @ExceptionHandler(NotFoundException.class)
  @ResponseStatus(HttpStatus.NOT_FOUND)
  default void handleNotFoundException() {
  }
}
