package it.dedagroup.stealth.digitalacademy2020.restapi.security.permissionevaluator;

public interface PermissionEvaluator extends org.springframework.security.access.PermissionEvaluator {
  boolean canManage(String targetType);
}
