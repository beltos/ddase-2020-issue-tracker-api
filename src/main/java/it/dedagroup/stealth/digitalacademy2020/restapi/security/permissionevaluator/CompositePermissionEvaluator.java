package it.dedagroup.stealth.digitalacademy2020.restapi.security.permissionevaluator;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.List;

@Component
@ConditionalOnBean(PermissionEvaluator.class)
public class CompositePermissionEvaluator implements org.springframework.security.access.PermissionEvaluator {

  private final List<PermissionEvaluator> permissionEvaluators;

  public CompositePermissionEvaluator(List<PermissionEvaluator> permissionEvaluators) {
    this.permissionEvaluators = permissionEvaluators;
  }

  @Override
  public boolean hasPermission(Authentication authentication, Object targetDomainObject, Object permission) {
    return permissionEvaluators.stream()
        .filter(pe -> pe.canManage(targetDomainObject.getClass().getCanonicalName()))
        .allMatch(pe -> pe.hasPermission(authentication, targetDomainObject, permission));
  }

  @Override
  public boolean hasPermission(Authentication authentication, Serializable targetId, String targetType, Object permission) {
    return permissionEvaluators.stream()
        .filter(pe -> pe.canManage(targetType))
        .allMatch(pe -> pe.hasPermission(authentication, targetId, targetType, permission));
  }
}