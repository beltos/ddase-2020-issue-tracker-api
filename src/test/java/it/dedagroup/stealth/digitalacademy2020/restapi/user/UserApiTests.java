package it.dedagroup.stealth.digitalacademy2020.restapi.user;

import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import javax.transaction.Transactional;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@WithMockUser
public class UserApiTests {
  @Autowired
  private MockMvc mvc;

  private final ObjectMapper objectMapper;

  @Autowired
  private UserRepository repository;

  public UserApiTests() {
    this.objectMapper = createObjectMapper();
  }

  private ObjectMapper createObjectMapper() {
    ObjectMapper objectMapper = new ObjectMapper();
    objectMapper.disable(MapperFeature.USE_ANNOTATIONS);
    return objectMapper;
  }

  @Test
  public void givenNoUsers_whenGetAll_returnsEmptyList() throws Exception {
    MvcResult result = mvc.perform(get("/v1/users"))
        .andExpect(status().isOk())
        .andReturn();
    String resultJson = result.getResponse().getContentAsString();
    List<?> list = objectMapper.readValue(resultJson, List.class);
    Assertions.assertThat(list).isEmpty();
  }

  @Test
  @Transactional
  public void givenOneUser_whenGetAll_returnsOneElement() throws Exception {
    User user = createUser("admin");

    MvcResult result = mvc.perform(get("/v1/users"))
        .andExpect(status().isOk())
        .andReturn();
    String resultJson = result.getResponse().getContentAsString();
    List<?> list = objectMapper.readValue(resultJson, List.class);
    Assertions.assertThat(list).hasSize(1);
  }

  @Test
  @Transactional
  public void givenOneUser_whenGetAllWithFilterUsingUnmatchingString_returnsOneElement() throws Exception {
    User user = createUser("admin", "Luke", "Skywalker");

    MvcResult result = mvc.perform(get("/v1/users").param("filter", "user"))
        .andExpect(status().isOk())
        .andReturn();
    String resultJson = result.getResponse().getContentAsString();
    List<?> list = objectMapper.readValue(resultJson, List.class);
    Assertions.assertThat(list).isEmpty();
  }

  @Test
  @Transactional
  public void givenOneUser_whenGetAllWithFilterUsingUsername_returnsOneElement() throws Exception {
    User user = createUser("admin", "Luke", "Skywalker");

    MvcResult result = mvc.perform(get("/v1/users").param("filter", "admin"))
        .andExpect(status().isOk())
        .andReturn();
    String resultJson = result.getResponse().getContentAsString();
    List<?> list = objectMapper.readValue(resultJson, List.class);
    Assertions.assertThat(list).hasSize(1);
  }

  @Test
  @Transactional
  public void givenOneUser_whenGetAllWithFilterUsingUsernameSubstring_returnsOneElement() throws Exception {
    User user = createUser("admin", "Luke", "Skywalker");

    MvcResult result = mvc.perform(get("/v1/users").param("filter", "adm"))
        .andExpect(status().isOk())
        .andReturn();
    String resultJson = result.getResponse().getContentAsString();
    List<?> list = objectMapper.readValue(resultJson, List.class);
    Assertions.assertThat(list).hasSize(1);
  }

  @Test
  @Transactional
  public void givenOneUserWithCapitalFirstName_whenGetAllWithFilterUsingFirstNameLowercaseSubstring_returnsOneElement() throws Exception {
    User user = createUser("admin", "Luke", "Skywalker");

    MvcResult result = mvc.perform(get("/v1/users").param("filter", "luk"))
        .andExpect(status().isOk())
        .andReturn();
    String resultJson = result.getResponse().getContentAsString();
    List<?> list = objectMapper.readValue(resultJson, List.class);
    Assertions.assertThat(list).hasSize(1);
  }

  @Test
  @Transactional
  public void givenOneUserWithCapitalLastName_whenGetAllWithFilterUsingLastNameLowercaseSubstring_returnsOneElement() throws Exception {
    User user = createUser("admin", "Luke", "Skywalker");

    MvcResult result = mvc.perform(get("/v1/users").param("filter", "skywalk"))
        .andExpect(status().isOk())
        .andReturn();
    String resultJson = result.getResponse().getContentAsString();
    List<?> list = objectMapper.readValue(resultJson, List.class);
    Assertions.assertThat(list).hasSize(1);
  }

  @Test
  @Transactional
  public void givenOneUser_whenGetAllWithFilterUsingUsernameAndFirstName_returnsOneElement() throws Exception {
    User user = createUser("admin", "Luke", "Skywalker");

    MvcResult result = mvc.perform(get("/v1/users").param("filter", "admin luke"))
        .andExpect(status().isOk())
        .andReturn();
    String resultJson = result.getResponse().getContentAsString();
    List<?> list = objectMapper.readValue(resultJson, List.class);
    Assertions.assertThat(list).hasSize(1);
  }

  @Test
  @Transactional
  public void givenOneUser_whenGetAllWithFilterUsingFirstNameAndLastName_returnsOneElement() throws Exception {
    User user = createUser("admin", "Luke", "Skywalker");

    MvcResult result = mvc.perform(get("/v1/users").param("filter", "luke skywalker"))
        .andExpect(status().isOk())
        .andReturn();
    String resultJson = result.getResponse().getContentAsString();
    List<?> list = objectMapper.readValue(resultJson, List.class);
    Assertions.assertThat(list).hasSize(1);
  }

  @Test
  @Transactional
  public void givenOneUser_whenGetOne_returnsOk() throws Exception {
    User user = createUser("admin");

    mvc.perform(get("/v1/users/{id}", user.getId()))
        .andExpect(status().isOk());
  }

  @Test
  public void givenNoUsers_whenGetOne_returnsNotFound() throws Exception {
    mvc.perform(get("/v1/users/{id}", 1))
        .andExpect(status().isNotFound());
  }

  @Test
  @Transactional
  public void givenNoUsers_whenPost_returnsCreatedAndUserIsCreated() throws Exception {
    UserDto dto = createUserDto("admin");
    mvc.perform(post("/v1/users")
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(dto)))
        .andExpect(status().isCreated());

    Assertions.assertThat(repository.count()).isEqualTo(1);
  }

  @Test
  @Transactional
  public void givenOneUser_whenPutWithDifferentUser_returnsForbiddenAndUserIsNotUpdated() throws Exception {
    User user = createUser("admin");

    UserDto dto = createUserDto("updated");
    mvc.perform(put("/v1/users/{id}", user.getId())
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(dto)))
        .andExpect(status().isForbidden());

    Assertions.assertThat(repository.getOne(user.getId()).getUsername()).isEqualTo("admin");
  }

  @Test
  @Transactional
  @WithMockUser("admin")
  public void givenOneUser_whenPutWithTheSameUser_returnsOkAndUserIsUpdated() throws Exception {
    User user = createUser("admin");

    UserDto dto = createUserDto("updated");
    mvc.perform(put("/v1/users/{id}", user.getId())
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(dto)))
        .andExpect(status().isOk());

    Assertions.assertThat(repository.getOne(user.getId()).getUsername()).isEqualTo("updated");
  }

  @Test
  @Transactional
  public void givenNoUsers_whenPut_returnsNotFound() throws Exception {
    UserDto dto = createUserDto("updated");
    mvc.perform(put("/v1/users/{id}", 1)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(dto)))
        .andExpect(status().isNotFound());
  }

  @Test
  @Transactional
  public void givenOneUser_whenPostWithExistingUsername_returnsConflictAndUserIsNotCreated() throws Exception {
    User user1 = createUser("admin");

    UserDto dto = createUserDto(user1.getUsername());
    mvc.perform(post("/v1/users")
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(dto)))
        .andExpect(status().isConflict());

    Assertions.assertThat(repository.count()).isEqualTo(1);
  }

  @Test
  @Transactional
  @WithMockUser("another")
  public void givenTwoUsers_whenPutWithExistingUsername_returnsConflictAndUserIsNotUpdated() throws Exception {
    User user1 = createUser("admin");
    User user2 = createUser("another");

    UserDto dto = createUserDto(user1.getUsername());
    mvc.perform(put("/v1/users/{id}", user2.getId())
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(dto)))
        .andExpect(status().isConflict());

    Assertions.assertThat(repository.getOne(user2.getId()).getUsername()).isEqualTo("another");
  }

  private User createUser(String username) {
    return createUser(username, username, username);
  }

  private User createUser(String username, String firstName, String lastName) {
    User user = new User();
    user.setUsername(username);
    user.setPassword("password123");
    user.setFirstName(firstName);
    user.setLastName(lastName);
    repository.save(user);
    return user;
  }

  private UserDto createUserDto(String username) {
    UserDto dto = new UserDto();
    dto.setUsername(username);
    dto.setPassword("password123");
    dto.setFirstName(username);
    dto.setLastName(username);
    return dto;
  }
}
