package it.dedagroup.stealth.digitalacademy2020.restapi.issue;

import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import it.dedagroup.stealth.digitalacademy2020.restapi.user.User;
import it.dedagroup.stealth.digitalacademy2020.restapi.user.UserRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import javax.transaction.Transactional;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@WithMockUser(IssueApiTests.MOCKUSER_USERNAME)
public class IssueApiTests {

  public static final String MOCKUSER_USERNAME = "mockuser";

  @Autowired
  private MockMvc mvc;

  private final ObjectMapper objectMapper;

  @Autowired
  private IssueRepository repository;
  @Autowired
  private UserRepository userRepository;

  private User mockUser;

  public IssueApiTests() {
    this.objectMapper = createObjectMapper();
  }

  private ObjectMapper createObjectMapper() {
    ObjectMapper objectMapper = new ObjectMapper();
    objectMapper.registerModule(new Jdk8Module());
    objectMapper.registerModule(new JavaTimeModule());
    objectMapper.disable(MapperFeature.USE_ANNOTATIONS);
    objectMapper.enable(MapperFeature.ACCEPT_CASE_INSENSITIVE_ENUMS);
    objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    return objectMapper;
  }

  @BeforeEach
  public void setUp() {
    mockUser = createUser(MOCKUSER_USERNAME);
  }

  @AfterEach
  public void tearDown() {
    userRepository.delete(mockUser);
  }

  @Test
  public void givenNoIssues_whenGetAll_returnsEmptyList() throws Exception {
    MvcResult result = mvc.perform(get("/v1/issues"))
        .andExpect(status().isOk())
        .andReturn();
    String resultJson = result.getResponse().getContentAsString();
    List<?> list = objectMapper.readValue(resultJson, List.class);
    Assertions.assertThat(list).isEmpty();
  }

  @Test
  @Transactional
  public void givenOneIssue_whenGetAll_returnsOneElement() throws Exception {
    Issue issue = createIssue("test");

    MvcResult result = mvc.perform(get("/v1/issues"))
        .andExpect(status().isOk())
        .andReturn();
    String resultJson = result.getResponse().getContentAsString();
    List<?> list = objectMapper.readValue(resultJson, List.class);
    Assertions.assertThat(list).hasSize(1);
  }

  @Test
  @Transactional
  public void givenOneIssue_whenGetAllWithStatusOpen_returnsOneElement() throws Exception {
    Issue issue = createIssue("test");

    MvcResult result = mvc.perform(get("/v1/issues")
        .queryParam("status", IssueStatus.OPEN.toString()))
        .andExpect(status().isOk())
        .andReturn();
    String resultJson = result.getResponse().getContentAsString();
    List<?> list = objectMapper.readValue(resultJson, List.class);
    Assertions.assertThat(list).hasSize(1);
  }

  @Test
  @Transactional
  public void givenOneIssue_whenGetAllWithStatusClosed_returnsEmptyList() throws Exception {
    Issue issue = createIssue("test");

    MvcResult result = mvc.perform(get("/v1/issues")
        .queryParam("status", IssueStatus.CLOSED.toString()))
        .andExpect(status().isOk())
        .andReturn();
    String resultJson = result.getResponse().getContentAsString();
    List<?> list = objectMapper.readValue(resultJson, List.class);
    Assertions.assertThat(list).isEmpty();
  }

  @Test
  @Transactional
  public void givenOneIssue_whenGetAllWithCorrectReporterId_returnsOneElement() throws Exception {
    Issue issue = createIssue("test");

    MvcResult result = mvc.perform(get("/v1/issues")
        .queryParam("reporterId", mockUser.getId().toString()))
        .andExpect(status().isOk())
        .andReturn();
    String resultJson = result.getResponse().getContentAsString();
    List<?> list = objectMapper.readValue(resultJson, List.class);
    Assertions.assertThat(list).hasSize(1);
  }

  @Test
  @Transactional
  public void givenOneIssue_whenGetAllWithWrongReporterId_returnsEmptyList() throws Exception {
    Issue issue = createIssue("test");

    MvcResult result = mvc.perform(get("/v1/issues")
        .queryParam("reporterId", "-1"))
        .andExpect(status().isOk())
        .andReturn();
    String resultJson = result.getResponse().getContentAsString();
    List<?> list = objectMapper.readValue(resultJson, List.class);
    Assertions.assertThat(list).isEmpty();
  }

  @Test
  @Transactional
  public void givenOneIssueWithAssignee_whenGetAllWithCorrectAssigneeId_returnsOneElement() throws Exception {
    Issue issue = createIssue("test", mockUser);

    MvcResult result = mvc.perform(get("/v1/issues")
        .queryParam("assigneeId", mockUser.getId().toString()))
        .andExpect(status().isOk())
        .andReturn();
    String resultJson = result.getResponse().getContentAsString();
    List<?> list = objectMapper.readValue(resultJson, List.class);
    Assertions.assertThat(list).hasSize(1);
  }

  @Test
  @Transactional
  public void givenOneIssueWithoutAssignee_whenGetAllWithAnyAssigneeId_returnsEmptyList() throws Exception {
    Issue issue = createIssue("test");

    MvcResult result = mvc.perform(get("/v1/issues")
        .queryParam("assigneeId", mockUser.getId().toString()))
        .andExpect(status().isOk())
        .andReturn();
    String resultJson = result.getResponse().getContentAsString();
    List<?> list = objectMapper.readValue(resultJson, List.class);
    Assertions.assertThat(list).isEmpty();
  }

  @Test
  @Transactional
  public void givenOneIssue_whenGetOne_returnsOk() throws Exception {
    Issue issue = createIssue("test");

    mvc.perform(get("/v1/issues/{id}", issue.getId()))
        .andExpect(status().isOk());
  }

  @Test
  public void givenNoIssues_whenGetOne_returnsNotFound() throws Exception {
    mvc.perform(get("/v1/issues/{id}", 1))
        .andExpect(status().isNotFound());
  }

  @Test
  @Transactional
  public void givenNoIssues_whenPost_returnsCreatedAndIssueIsCreated() throws Exception {
    IssueDto dto = createIssueDto("test");
    mvc.perform(post("/v1/issues")
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(dto)))
        .andExpect(status().isCreated());

    Assertions.assertThat(repository.count()).isEqualTo(1);
  }

  @Test
  @Transactional
  public void givenOneIssue_whenPutWithUserReporter_returnsOkAndIssueIsUpdated() throws Exception {
    Issue issue = createIssue("test");

    IssueDto dto = createIssueDto("updated");
    mvc.perform(put("/v1/issues/{id}", issue.getId())
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(dto)))
        .andExpect(status().isOk());

    Assertions.assertThat(repository.getOne(issue.getId()).getTitle()).isEqualTo("updated");
  }

  @Test
  @Transactional
  @WithMockUser("assignee")
  public void givenOneIssueWithAssignee_whenPutWithUserAssignee_returnsOkAndIssueIsUpdated() throws Exception {
    User reporter = createUser("reporter");
    User assignee = createUser("assignee");
    Issue issue = createIssue("test", reporter, assignee);

    IssueDto dto = createIssueDto("updated");
    mvc.perform(put("/v1/issues/{id}", issue.getId())
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(dto)))
        .andExpect(status().isOk());

    Assertions.assertThat(repository.getOne(issue.getId()).getTitle()).isEqualTo("updated");
  }

  @Test
  @Transactional
  public void givenOneIssue_whenPutWithUserNotReporterAndNotAssignee_returnsForbiddenAndIssueIsNotUpdated() throws Exception {
    User reporter = createUser("reporter");
    User assignee = createUser("assignee");
    Issue issue = createIssue("test", reporter, assignee);

    IssueDto dto = createIssueDto("updated");
    mvc.perform(put("/v1/issues/{id}", issue.getId())
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(dto)))
        .andExpect(status().isForbidden());

    Assertions.assertThat(repository.getOne(issue.getId()).getTitle()).isEqualTo("test");
  }

  @Test
  @Transactional
  public void givenOneIssue_whenDelete_returnsNoContentAndIssueIsDeleted() throws Exception {
    Issue issue = createIssue("test");

    mvc.perform(delete("/v1/issues/{id}", issue.getId()))
        .andExpect(status().isNoContent());

    Assertions.assertThat(repository.count()).isEqualTo(0);
  }

  @Test
  @Transactional
  @WithMockUser("another")
  public void givenOneIssue_whenDeleteWithUserNotReporter_returnsForbiddenAndIssueIsNotDeleted() throws Exception {
    Issue issue = createIssue("test");

    mvc.perform(delete("/v1/issues/{id}", issue.getId()))
        .andExpect(status().isForbidden());

    Assertions.assertThat(repository.count()).isEqualTo(1);
  }

  @Test
  @Transactional
  public void givenNoIssues_whenPut_returnsNotFound() throws Exception {
    IssueDto dto = createIssueDto("updated");
    mvc.perform(put("/v1/issues/{id}", 1)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(dto)))
        .andExpect(status().isNotFound());
  }

  @Test
  @Transactional
  public void givenNoIssues_whenDelete_returnsNotFound() throws Exception {
    mvc.perform(delete("/v1/issues/{id}", 1))
        .andExpect(status().isNotFound());
  }

  @Test
  @Transactional
  public void givenOneIssue_whenPutWithNonExistingAssignee_returns461AndIssueIsNotUpdated() throws Exception {
    Issue issue = createIssue("test");

    IssueDto dto = createIssueDto("updated", 1L);
    mvc.perform(put("/v1/issues/{id}", issue.getId())
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(dto)))
        .andExpect(status().is(461));

    Issue issueFromDb = repository.getOne(issue.getId());
    Assertions.assertThat(issueFromDb.getTitle()).isEqualTo("test");
    Assertions.assertThat(issueFromDb.getAssignee()).isNull();
  }

  @Test
  @Transactional
  public void givenOneIssue_whenPutWithExistingAssignee_returnsOkAndIssueIsUpdated() throws Exception {
    Issue issue = createIssue("test");
    User user = createUser("test");

    IssueDto dto = createIssueDto("updated", user.getId());
    mvc.perform(put("/v1/issues/{id}", issue.getId())
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(dto)))
        .andExpect(status().isOk());

    Issue issueFromDb = repository.getOne(issue.getId());
    Assertions.assertThat(issueFromDb.getTitle()).isEqualTo("updated");
    Assertions.assertThat(issueFromDb.getAssignee()).isNotNull();
    Assertions.assertThat(issueFromDb.getAssignee().getId()).isEqualTo(user.getId());
  }

  @Test
  @Transactional
  public void givenNewIssueAndExistingMockUser_whenPost_returnsOkAndAuditingFieldsArePopulatedAutomatically() throws Exception {
    IssueDto dto = createIssueDto("test");
    MvcResult mvcResult = mvc.perform(post("/v1/issues")
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(dto)))
        .andExpect(status().isCreated())
        .andReturn();
    String resultJson = mvcResult.getResponse().getContentAsString();
    IssueDto resultDto = objectMapper.readValue(resultJson, IssueDto.class);

    Assertions.assertThat(resultDto.getReporterId()).isEqualTo(mockUser.getId());
    Assertions.assertThat(resultDto.getCreatedAt()).isNotNull();
    Assertions.assertThat(resultDto.getUpdatedAt()).isNotNull();
  }


  private Issue createIssue(String title) {
    return createIssue(title, null);
  }

  private Issue createIssue(String title, User assignee) {
    return createIssue(title, mockUser, assignee);
  }

  private Issue createIssue(String title, User reporter, User assignee) {
    Issue issue = new Issue();
    issue.setTitle(title);
    issue.setDescription(title);
    issue.setStatus(IssueStatus.OPEN);
    issue.setReporter(reporter);
    issue.setAssignee(assignee);
    repository.save(issue);
    return issue;
  }

  private IssueDto createIssueDto(String title) {
    return createIssueDto(title, null);
  }

  private IssueDto createIssueDto(String title, Long assigneeId) {
    IssueDto dto = new IssueDto();
    dto.setTitle(title);
    dto.setDescription(title);
    dto.setStatus(IssueStatus.OPEN);
    dto.setAssigneeId(assigneeId);
    return dto;
  }

  private User createUser(String username) {
    User user = new User();
    user.setUsername(username);
    user.setPassword("password123");
    user.setFirstName(username);
    user.setLastName(username);
    userRepository.saveAndFlush(user);
    return user;
  }
}
