package it.dedagroup.stealth.digitalacademy2020.restapi.openapi;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import static it.dedagroup.stealth.digitalacademy2020.restapi.openapi.ApiDocsConstants.API_DOCS_YML_RESOURCE_PATH;

public class OpenApiDocsValidationTests {
  @Test
  public void openApiDefinitionIsValid() throws Exception {
    openApiDefinitionIsValid(new ClassPathResource(API_DOCS_YML_RESOURCE_PATH));
  }

  @SneakyThrows
  private void openApiDefinitionIsValid(Resource resource) {
//    System.out.println("Testing " + resource.getFilename());
    OpenApiUtils.readOpenApi(resource.getInputStream());
  }
}
